package com.eddie.replication.server.handler;

import com.eddie.replication.server.client.SocketClient;

import lombok.extern.slf4j.Slf4j;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.stream.Consumer;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.stream.StreamMessageListenerContainer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;


@Slf4j
@Component
public class ConcurrentEditHandler  implements WebSocketHandler {

    static Map<String, SocketClient> clients = new ConcurrentHashMap<>();

    List<String> streams = new ArrayList<>();

    @Value("{server.port}")
    String port;
    @Override
    public Mono<Void> handle(WebSocketSession session) {

        String streamId = "eddie tsang";

        String id = session.getId();
        try {
            Mono<Void> output = session.send(Flux.create(sink -> clients.put(id, new SocketClient(streamId, sink,session))));
            Mono<Void> input = session.receive()
                    .map(WebSocketMessage::getPayloadAsText).map(msg ->  msg)
                    .doOnNext(msg-> {
                        try {
                            sendOthers(streamId,id,msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).then();

            return  Mono.zip(  input,output).doFinally(signal -> {
                clients.remove(id);
            }).then();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Map<String, SocketClient> getSessions() {
        return clients;
    }

    public void sendOthers(String streamId,String id,String message) throws Exception {
        Map<String, SocketClient> sessions = getSessions();
        JSONObject jsonobj = new JSONObject(message);
        System.out.println("command: " + jsonobj.getString("command"));
        System.out.println(message);
        try {
            String outcome = commandFactory(jsonobj, sessions, message, id);
            if (outcome.isEmpty()) {
                outcome = message;
            }
            String finalOutcome = outcome;
            sessions.forEach((sessionId, client) -> {
                if(client.getDocRefNo() != null && client.getDocRefNo().equalsIgnoreCase(jsonobj.getString("ref_no"))) {
                    client.sendData("Server Message: "+ finalOutcome);
                }
            });
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public String commandFactory(JSONObject obj, Map<String, SocketClient> sessions, String message, String sessionId) throws Exception {
        StringBuffer outcome = new StringBuffer();
        SocketClient currentClient = sessions.get(sessionId);
        JSONObject user = currentClient.getUserProfile();
        try {
            switch(obj.getString("command")) {
                case "/websocket/concurrent-edit/onlineuser/register":
                    currentClient.setUserProfile(obj.getJSONObject("user"));
                    currentClient.setDocRefNo(obj.getString("ref_no"));
                    outcome.append(getOnlineUsers(obj, sessions, message, sessionId).toString());
                    break;
                case "/websocket/concurrent-edit/onlineuser/list":
                    outcome.append(getOnlineUsers(obj, sessions, message, sessionId).toString());
                    break;
                case "/websocket/concurrent-edit/onlineuser/edit/start":
                    user = currentClient.getUserProfile();
                    user.put("read", false);
                    user.put("edit", true);
                    outcome.append(getOnlineUsers(obj, sessions, message, sessionId).toString());
                    break;
                case "/websocket/concurrent-edit/onlineuser/edit/stop":
                    user = currentClient.getUserProfile();
                    user.put("read", true);
                    user.put("edit", false);
                    outcome.append(getOnlineUsers(obj, sessions, message, sessionId).toString());
                    break;
                case "/websocket/concurrent-edit/document/amend/field/update":
                    outcome.append(obj.toString());
                    break;
                case "/websocket/concurrent-edit/document/amend/field/cursor/movement":
                    System.out.println("moving cursor....");
                    outcome.append(obj.toString());
                    break;
                case "/websocket/concurrent-edit/document/amend/save":
                    //update redis temporary document data and broadcast the update to all client.
                    outcome.append(updateRedisAfterDocSave(obj, sessions, message, sessionId).toString());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outcome.toString();
    }

    public JSONObject getOnlineUsers(JSONObject obj, Map<String, SocketClient> sessions, String message, String sessionId) throws Exception {
        JSONObject output = new JSONObject();
        JSONArray jsArray = new JSONArray();
        try {
            sessions.forEach((sessionsId,client) -> {
                if (client.getDocRefNo() != null && client.getDocRefNo().equalsIgnoreCase(obj.getString("ref_no"))) {
                    jsArray.put(client.getUserProfile());
                }
            });
            output.put("user_list", jsArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public JSONObject handleModification(JSONObject obj, Map<String, SocketClient> sessions, String message, String sessionId) {
        // modify something on redis
        return obj;
    }

    public void loggingUserDocActivities() {
        // saving user modification actions.
    }

    public JSONObject updateRedisAfterDocSave(JSONObject obj, Map<String, SocketClient> sessions, String message, String sessionId) throws Exception {
        return obj;
    }

    public void handleDisconnection(String sessionId) {
        Map<String, SocketClient> sessions = getSessions();
        sessions.remove(sessionId);

    }
}

