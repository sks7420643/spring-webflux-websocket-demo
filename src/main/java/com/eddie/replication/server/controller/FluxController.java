package com.eddie.replication.server.controller;

import com.eddie.replication.server.client.SocketClient;
import com.eddie.replication.server.handler.ConcurrentEditHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;

import java.util.Map;


@Slf4j
@RestController
public class FluxController {

    @Autowired
    ConcurrentEditHandler concurrentEditHandler;
    @GetMapping("/sendMessage")
    public String sendMessage(String message) {
        Map<String, SocketClient> sessions = ConcurrentEditHandler.getSessions();

        sessions.forEach((sessionId,client) ->{
            client.sendData(message);
        });
        int size = sessions.size();
        return size+"";
    }
}
