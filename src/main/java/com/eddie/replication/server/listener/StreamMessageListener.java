package com.eddie.replication.server.listener;

import com.eddie.replication.server.client.SocketClient;
import com.eddie.replication.server.handler.ConcurrentEditHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.stream.StreamListener;
import org.springframework.stereotype.Component;

import java.util.Map;


@Slf4j
@Component
public class StreamMessageListener implements StreamListener<String, MapRecord<String, String, String>> {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Override
    public void onMessage(MapRecord<String, String, String> message) {

        RecordId messageId = message.getId();

        Map<String, String> body = message.getValue();

        //log.info("stream message。messageId={}, stream={}, body={}", messageId, message.getStream(), body);

        Map<String, SocketClient> sessions = ConcurrentEditHandler.getSessions();
        sessions.forEach((id,client) ->
            client.sendData("Server Message: "+body)
        );

        this.stringRedisTemplate.opsForStream().acknowledge(message.getStream(), message);
    }
}
